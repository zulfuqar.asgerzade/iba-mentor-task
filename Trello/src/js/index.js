// DELETE CARD
function deleteCard(event) {
    event.closest('.card').remove();
}



// ADD CARD
function addCard(event) {
    let newCard = document.createElement('div');
    newCard.classList.add('card');
    newCard.innerHTML += '<div class="card_headerContainer"><input type="text" class="card_headerInput" value="Default"><button class="card_deleteBtn"><i class="fas fa-trash"></i></button></div><button class="card_addTaskBtn"><i class="fas fa-plus card_plusIcon"></i>Add a task</button>';
    event.before(newCard);
}



// ADD TASK
function addTask(event) {

    let taskContainer = document.createElement('div');
    taskContainer.innerHTML = '<i class="fas fa-trash card_deleteTaskBtn"></i><input type="text" class="card_task">';
    taskContainer.classList.add('card_taskContainer')
    
    event.before(taskContainer);
}


// DELETE TASK
function deleteTask(event) {
    event.closest('.card_taskContainer').remove();
}


// CHANGE CARD HEADER
function changeCardHeader() {
    let cardHeader = document.querySelectorAll('.card_headerInput');
    
    for (let i = 0; i < cardHeader.length; i++) {
        
        cardHeader[i].addEventListener('change', () => {
            storeData();
        });
    }
}


// CREATE CARD
function createCard() {

    let addCardBtn = document.querySelector('.card_addCardBtn');
    
    for (let i = 0; i < Object.keys(cardData).length; i++) {
        
        let newCard = document.createElement('div');
        newCard.classList.add('card');
        newCard.innerHTML += `<div class="card_headerContainer"><input type="text" class="card_headerInput" value="${Object.keys(cardData)[i]}"><button class="card_deleteBtn"><i class="fas fa-trash"></i></button></div><button class="card_addTaskBtn"><i class="fas fa-plus card_plusIcon"></i>Add a task</button>`;
        addCardBtn.before(newCard);
    }
}


// COLLECT ALL TASKS
let allTask = [];

function collectTasks() {

    allTask = [];

    let taskContainer = document.querySelectorAll('.card');

    for (let i = 0; i < taskContainer.length; i++) {
        
        let tasksInput = taskContainer[i].querySelectorAll('.card_task');

        let tasks = [];
        
        for (let i = 0; i < tasksInput.length; i++) {
            tasks.push(tasksInput[i].value);
        }

        allTask.push(tasks);
    }
    
    console.log("All TASKS");
    console.log(allTask);
}


// STORE DATA TO LOCALSTORAGE
let cardData = {};

function storeData() {

    let counter = 0;

    cardData = {};

    let cards = document.querySelectorAll('.card_headerInput');
    
    for (let i = 0; i < cards.length; i++) {
        let key = cards[i].value;
        cardData[key] = {};
    }

    console.log(cardData);

    localStorage.setItem('cardsDataObj', JSON.stringify(cardData));
}


// READ FROM LOCALSTORAGE
function readLocalstorage() {
    cardData = JSON.parse(localStorage.getItem('cardsDataObj'));
}


// WIN ONLOAD
window.onload = () => {
    readLocalstorage();
    createCard();
    changeCardHeader(); 
}



let mainContainer = document.querySelector('.mainContainer');
mainContainer.addEventListener('click', (event) => {

    if(event.target.className == 'card_deleteBtn' || event.target.className == 'fas fa-trash') {
        deleteCard(event.target);
        storeData();
    }
    else if(event.target.className == 'card_addTaskBtn') {
        addTask(event.target);
        storeData();
        collectTasks();
    }
    else if(event.target.className == 'card_addCardBtn') {
        addCard(event.target);
        storeData();
        changeCardHeader();
    }
    else if(event.target.classList.contains('card_deleteTaskBtn')) {
        deleteTask(event.target);
    }
});


