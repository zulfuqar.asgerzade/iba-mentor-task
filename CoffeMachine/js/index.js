function Machine(name, date, owner, repairs) {
    this.name = name;
    this.date = date;
    this.owner = owner;
    this.repairs = repairs;
}

function CoffeeMachine(picture, coffeeMenu, prepareBtn) {
    this.picture = picture;
    this.coffeeMenu = coffeeMenu;
    this.prepareBtn = prepareBtn;

    this.prepareCoffee = (coffee) => {
        if(coffee == 'Select your option') {
            alert('Firts you have to select coffee type');
        }
        else {
            for (let i = 0; i < drinks.length; i++) {
                if(drinks[i].name == coffee) {
                    // You can check other supplies like milk, water in here
                    if(drinks[i].recipe.sugar <= stockSupplies.sugar) {
                        let moneyInput = document.querySelector('.moneyInput');
                        if(drinks[i].price <= moneyInput.value) {
                            alert(`Here is your ${drinks[i].name}`);
                        }
                        else {
                            alert('Your payment is not enough!');
                        }
                    }
                    else {
                        alert('There is no enough supplies for cook it! Sorry...');
                    }
                }
            }
        }
    }
}

function Recipe(sugar, milk, water) {
    this.sugar = sugar;
    this.milk = milk;
    this.water = water;
}

function Drink(name, recipe, price) {
    this.name = name;
    this.recipe = recipe;
    this.price = price;
}

let stockSupplies = {
    sugar: 1,
    milk: 2,
    water: 3
}


let drinks = [
    new Drink('Coffee1', new Recipe(1, 1, 1), 1),
    new Drink('Coffee2', new Recipe(3, 3, 3), 2),
    new Drink('Coffee3', new Recipe(4, 4, 4), 3),
];


// Fill coffees to dropdown menu
window.onload = () => {
    for (let i = 0; i < drinks.length; i++) {
        let coffeeOption = document.createElement('option');
        coffeeOption.textContent = drinks[i].name;
        document.querySelector('.coffeeMenu').appendChild(coffeeOption);
    }
}




let today = new Date().getDate() + '/' + (new Date().getMonth() + 1) + '/' + new Date().getFullYear();
let machine = new Machine('MachineName', today, 'CustomCompany', '+994703651230');

let img = document.querySelector('.machinePhoto');
let dropDown = document.querySelector('.coffeeMenu');
let prepareBtn = document.querySelector('.prepareBtn');
let coffeeMachine = new CoffeeMachine(img, dropDown, prepareBtn);

prepareBtn.addEventListener('click', () => {
    coffeeMachine.prepareCoffee(dropDown[dropDown.selectedIndex].text);
});