let stockSupplies = {
  sugar: 1,
  milk: 2,
  water: 3
}

class Machine {
  constructor(name, date, owner, repairs) {
    this.name = name;
    this.date = date;
    this.owner = owner;
    this.repairs = repairs;
  }
}

class CoffeeMachine extends Machine {
  constructor(name, date, owner, repairs, drinks, picture, coffeeMenu, prepareBtn) {
    super(name, date, owner, repairs);
    this.drinks = drinks;
    this.picture = picture;
    this.coffeeMenu = coffeeMenu;
    this.prepareBtn = prepareBtn;
  }

  fillMachine() {
    for (let i = 0; i < this.drinks.length; i++) {
      let coffeeOption = document.createElement('option');
      coffeeOption.textContent = this.drinks[i].name;
      document.querySelector('.coffeeMenu').appendChild(coffeeOption);
    }
  }

  prepareCoffee() {
    this.prepareBtn.addEventListener('click', () => {
      if (this.coffeeMenu[this.coffeeMenu.selectedIndex].text == 'Select your option') {
        alert('Firts you have to select coffee type');
      }
      else {
        let coffee = this.drinks.find((item) => item.name === this.coffeeMenu[this.coffeeMenu.selectedIndex].text);
        // You can check other supplies like milk, water in here
        if(coffee.sugar <= stockSupplies.sugar) {
          let moneyInput = document.querySelector('.moneyInput');
          if(coffee.price <= moneyInput.value) {
              alert(`Here is your ${coffee.name}`);
          }
          else {
              alert('Your payment is not enough!');
          }
        }
        else {
          alert('There is no enough supplies for cook it! Sorry...');
        }
      }
    });
  }
}



let xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        let coffeeMachine = new CoffeeMachine(
          'Palma',
          '8/8/2020',
          'Businessman',
          '+994703651230',
          JSON.parse(xhttp.responseText),
          document.querySelector('.machinePhoto'),
          document.querySelector('.coffeeMenu'),
          document.querySelector('.prepareBtn')
          );
        
          coffeeMachine.fillMachine();
        coffeeMachine.prepareCoffee();
    }
};
xhttp.open("GET", "./json/recipe.json", false);
xhttp.send();